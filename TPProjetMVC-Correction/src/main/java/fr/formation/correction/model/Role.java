package fr.formation.correction.model;

public enum Role {
	ROLE_ADMIN,ROLE_USER;
	
	public String toString() {
		
		switch(this) {
		case ROLE_ADMIN :
			return "ROLE_ADMIN";
		case ROLE_USER :
			return "ROLE_USER";
		}
		return null;
	}
}

