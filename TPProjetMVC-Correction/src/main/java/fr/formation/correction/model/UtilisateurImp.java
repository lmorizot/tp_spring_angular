package fr.formation.correction.model;

import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.StringUtils;


@SuppressWarnings("serial")
public class UtilisateurImp implements UserDetails{
	

	private Utilisateur utilisateur;

	public UtilisateurImp(Utilisateur utilisateur) {
		if (utilisateur == null) {
			throw new UsernameNotFoundException("L'utilisateur n'existe pas.");
		}
		this.utilisateur = utilisateur;
	}


//	@Override
//	public Collection<? extends GrantedAuthority> getAuthorities() {	
//		List<GrantedAuthority> myAuthorities = new ArrayList<GrantedAuthority>();
//		myAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
//		return myAuthorities;
//	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return AuthorityUtils.commaSeparatedStringToAuthorityList(
				//StringUtils.collectionToCommaDelimitedString(utilisateur.getStringRoles()));
			 StringUtils.collectionToCommaDelimitedString(utilisateur.getStringRoles()));
	}

	@Override
	public String getPassword() {
		return new BCryptPasswordEncoder().encode(utilisateur.getPassword());
	}

	@Override
	public String getUsername() {
		return this.utilisateur.getIdentifiant();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}




}
