package fr.formation.correction.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import fr.formation.correction.controller.exception.BindStagiaireToFiliereException;
import fr.formation.correction.model.Filiere;
import fr.formation.correction.model.Module;
import fr.formation.correction.model.Personne;
import fr.formation.correction.model.dto.FiliereDTO;
import fr.formation.correction.repository.FiliereRepository;

@Service
public class FiliereService {
	
	@Autowired
	private FiliereRepository fr;
	
	@Autowired
	private PersonneService ps;

	public void create(Filiere f) {
		if(!f.getStagiaires().isEmpty() && !ps.checkAllStagiaire(f.getStagiaires())) throw new BindStagiaireToFiliereException();
		Filiere persistedFiliere = this.fr.save(f);
		
		if(!f.getStagiaires().isEmpty()) {
			for(Personne p : f.getStagiaires()) {
				ps.bindStagiaireToFiliere(p, persistedFiliere);
			}
		}
	}

	public List<Filiere> findAll() {
		return this.fr.findAll();
	}
	
	public Optional<Filiere> getById(Integer id) {
		return this.fr.findById(id);
	}
	
	public void update(Filiere f) {
		this.fr.save(f);
	}
	
	public Optional<Boolean> delete(Filiere f) {
		try {
			this.fr.delete(f);
			return Optional.of(Boolean.TRUE);
		} catch(EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}
	
	public Optional<Boolean> delete(Integer id) {
		try {
			this.fr.deleteById(id);
			return Optional.of(Boolean.TRUE);
		} catch(EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}
	
	public Optional<FiliereDTO> getByIdWithDetail(Integer id) {
		Optional<Filiere> optF = fr.findById(id);
		if(optF.isEmpty()) return Optional.empty();
		FiliereDTO dto = FiliereDTO.fromFiliere(optF.get());
		LocalDate minDate = null;
		LocalDate maxDate = null;
		for(Module m : dto.getModules()) {
			if(minDate == null || minDate.isAfter(m.getDateDebut())) minDate = m.getDateDebut();
			if(maxDate == null || maxDate.isBefore(m.getDateFin())) maxDate = m.getDateFin();
		}
		dto.setDateDebut(minDate);
		dto.setDateFin(maxDate);
		
		return Optional.of(dto);
	}
	
	public void setStagiairesOfFiliere(List<Personne> pList,Integer filiereId) {
		Filiere f = new Filiere();
		f.setId(filiereId);
		if(!ps.checkAllStagiaire(pList)) throw new BindStagiaireToFiliereException();
		ps.bindStagiairesToFiliere(pList, f);
	}}
