package fr.formation.correction.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import fr.formation.correction.model.Role;
import fr.formation.correction.model.RoleUtilisateur;


public interface RoleUtilisateurRepository extends JpaRepository<RoleUtilisateur, Integer> {
	@Query("select ru from RoleUtilisateur ru where ru.utilisateur.identifiant = :identifiant and ru.role = :role")
	Optional<RoleUtilisateur>findByUtilisateurAndRole(String identifiant, Role role);
}
