import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { FiliereDisplayComponent } from './filiere-display/filiere-display.component';
import { FiliereFormComponent } from './filiere-form/filiere-form.component';
import { FiliereModalComponent } from './filiere-modal/filiere-modal.component';

@NgModule({
  declarations: [
    FiliereFormComponent,
    FiliereDisplayComponent,
    FiliereModalComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  exports: [
    FiliereDisplayComponent,
  ],
})
export class FiliereModule { }
