import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiliereModalComponent } from './filiere-modal.component';

describe('FiliereModalComponent', () => {
  let component: FiliereModalComponent;
  let fixture: ComponentFixture<FiliereModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FiliereModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiliereModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
