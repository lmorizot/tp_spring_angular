import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { Filiere } from './filiere.interface';

const API = 'http://localhost:8080/api/filiere';

@Injectable({
  providedIn: 'root',
})
export class FiliereService {
  constructor(private http: HttpClient) {}

  getFilieres(): Observable<Filiere[]> {
    return this.http
      .get<Filiere[]>(API);
  }


  postFiliere(filiere: Filiere): Observable<HttpResponse<Filiere>> {
    const headers: HttpHeaders = new HttpHeaders()
    .set('Content-Type', 'application/json')
    return this.http.post<Filiere>(API, filiere, { headers, observe: 'response' });
  }

  updateFiliere(id: number, filiere: Filiere): Observable<HttpResponse<Filiere>> {
    const headers: HttpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json')

    return this.http.put<Filiere>(API+"/"+id, filiere, { headers, observe: 'response' });

  }

  deleteFiliere(id: number): Observable<HttpResponse<any>>{
    const headers: HttpHeaders = new HttpHeaders()
    .set('Content-Type', 'application/json');

    return this.http.delete<HttpResponse<any>>(API+"/"+id, { headers, observe: 'response' });
  }



}
