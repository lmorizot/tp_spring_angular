import { HttpResponse } from '@angular/common/http';
import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ConnectableObservable, Observable } from 'rxjs';
import { Filiere } from '../filiere.interface';
import { FiliereService } from '../filiere.service';

@Component({
  selector: 'app-filiere-form',
  templateUrl: './filiere-form.component.html',
  styleUrls: ['./filiere-form.component.scss']
})
export class FiliereFormComponent implements OnInit {

  filiereForm: FormGroup | null = null;

  @Input() filiere: Filiere = {
    libelle: '',
    stagiaires: [],
  };

  constructor(private filiereService: FiliereService) { }

  ngOnInit(): void {
    this.filiereForm = new FormGroup({
      libelle: new FormControl('', Validators.required)
    });
  }

  submit(): Observable<HttpResponse<Filiere>> {
    this.filiere.libelle = this.filiereForm?.value.libelle;
    return this.filiereService.postFiliere(this.filiere)
  }
}
