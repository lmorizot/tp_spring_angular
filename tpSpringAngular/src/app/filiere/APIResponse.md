````json
[
    {
        "id": 1,
        "libelle": "INFO",
        "modules": [
            {
                "id": 1,
                "libelle": "Projet Java",
                "dateDebut": "2022-02-25",
                "dateFin": "2022-02-28",
                "formateur": {
                    "id": 2,
                    "nom": "TITI",
                    "prenom": "titi",
                    "type": "FORMATEUR"
                }
            }
        ],
        "stagiaires": [
            {
                "id": 1,
                "nom": "TOTO",
                "prenom": "toto",
                "type": "STAGIAIRE",
                "modules": []
            }
        ]
    },
    {
        "id": 2,
        "libelle": "AGRI",
        "modules": [],
        "stagiaires": []
    }
]
````
