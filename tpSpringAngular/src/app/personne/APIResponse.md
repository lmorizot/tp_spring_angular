````json
[
    {
        "id": 1,
        "nom": "TOTO",
        "prenom": "toto",
        "type": "STAGIAIRE",
        "filiere": {
            "id": 1,
            "libelle": "INFO",
            "modules": [
                {
                    "id": 1,
                    "libelle": "Projet Java",
                    "dateDebut": "2022-02-25",
                    "dateFin": "2022-02-28",
                    "formateur": {
                        "id": 2,
                        "nom": "TITI",
                        "prenom": "titi",
                        "type": "FORMATEUR"
                    }
                }
            ]
        },
        "modules": []
    },
    {
        "id": 2,
        "nom": "TITI",
        "prenom": "titi",
        "type": "FORMATEUR",
        "filiere": null,
        "modules": [
            {
                "id": 1,
                "libelle": "Projet Java",
                "dateDebut": "2022-02-25",
                "dateFin": "2022-02-28"
            }
        ]
    }
]
````
